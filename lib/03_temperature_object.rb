class Temperature
  # TODO: your code goes here!
  attr_accessor :temp

  def ftoc(temp)
    5/9.0 * (temp - 32)
  end

  def ctof(temp)
    (9.0/5 * temp) + 32
  end

  def in_fahrenheit
    @temp
  end

  def in_celsius
    ftoc(@temp)
  end

  def initialize(options)
    if options[:f]
      @temp = options[:f]
    else
      @temp = ctof(options[:c])
    end
  end

  def self.from_fahrenheit(temp)
    Temperature.new({f: temp})
  end

  def self.from_celsius(temp)
    Temperature.new({c:temp})
  end
end

class Celsius < Temperature
  def initialize(temp)
    @temp = ctof(temp)
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @temp = temp
  end
end

class Book
  # TODO: your code goes here!
  attr_accessor :title

  def title=(title)
    @title = titleize(title)
  end

  def titleize(title)
    smalls = ["and", "a", "an", "the", "in", "of"]
    words = title.split
    words.map.with_index {|word, i|
      if i != 0 && smalls.include?(word)
        word
      else
        word.capitalize
      end
    }.join(" ")
  end
end

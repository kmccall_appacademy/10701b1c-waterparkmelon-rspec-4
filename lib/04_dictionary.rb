class Dictionary
  # TODO: your code goes here!
  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(entry)
    if entry.is_a? Hash
      @entries.merge!(entry)
    elsif entry.is_a? String
      @entries[entry] = nil
    end
  end

  def keywords
    self.entries.keys.sort
  end

  def include?(key)
    self.keywords.include?(key)
  end

  def find(str)
    self.entries.select {|key| key.include?(str)}.to_h
  end

  def printable
    prints = self.keywords.map {|k| %Q{[#{k}] "#{@entries[k]}"}}
    prints.join("\n")
  end
end

class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    hour = @seconds/3600
    minute = (@seconds%3600)/60
    second = @seconds%60
    "#{stringify(hour)}:#{stringify(minute)}:#{stringify(second)}"
  end

  def stringify(num)
    if num < 10
      "0#{num}"
    else
      "#{num}"
    end
  end
end
